[![crates.io](https://img.shields.io/crates/v/r2d2-mongodb.svg)](https://crates.io/crates/r2d2-mongodb)
[![docs.rs](https://docs.rs/r2d2-mongodb/badge.svg)](https://docs.rs/r2d2-mongodb)

# r2d2-mongodb (DEPRECATED!)

Driver in crate `mongodb` already does connection pooling internally. Use that one directly.

A MongoDB adaptor for r2d2 connection pool.

## Documentation

[Available here](https://docs.rs/r2d2-mongodb)

## Example usage

You can easily start a new mongo database using docker:

```shell
$ docker run --rm -p 27017:27017 mongo
```

Simple usage:

```rust
use r2d2::Pool;
use r2d2_mongodb::{MongoClientManager, MongoDbManager};

fn main () {
    // To manage just the client use:
    let manager = MongoClientManager::from_uri("mongodb://localhost:27017/").unwrap();
    // To manage a specific database use:
    let manager = MongoDbManager::from_uri("mongodb://localhost:27017/", "my_database").unwrap();

    let pool = Pool::builder()
        .max_size(8)
        .build(manager)
        .unwrap();

    // ...
}
```
